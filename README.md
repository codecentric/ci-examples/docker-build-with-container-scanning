# docker-build-with-container-scanning

A simple docker build (via kaniko) and push into the registry. Afterwards the image is scanned for vulnerabilities.

In GitLab Free (or CE) this requires GitLab Version >= 15.0.0
